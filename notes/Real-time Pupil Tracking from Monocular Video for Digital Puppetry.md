https://arxiv.org/pdf/2006.11341.pdf


# Opis: 
Praca przedstawia proces tworzenia animowanej nakładki na zdjęcie z kamery telefonu w czasie rzeczywistym. 

![](https://i.imgur.com/0bqMzJt.png)

Autorzy wykorzystują model do przewidywania punktów opisujących geometrię twarzy ([[#Face mesh]]). Przewidują położenie źrenicy za pomocą małej [[#Sieć neuronowa|sieci neuronowej]]. Estymują położenie źrenicy animowanej nakładki za pomocą [[#Estymacja przesunięcia źrenicy awatara|displacement based algorithm]].
Następnie opisany jest [[#Zbiór danych do trenowania sieci neuronowej|trening sieci]]. 

Graf opisujący proces otrzymywania nakładki 

![](https://i.imgur.com/4o7RGA6.png)

# Face mesh
https://arxiv.org/pdf/1907.06724.pdf

Model opisany jest w powyższym artykule. 
Opracowanie może być umieszczone w pliku o nazwie nawiązującej do tytułu: **Real-time Pupil Tracking from Monocular Video for Digital Puppetry**


# Sieć neuronowa 
Sieć neuronowa zwracająca tensor położenia punktów szczególnych oczu.

1. **Input** stanowi 468 punktów siatki 
2. Wycinamy region (tutaj 64 x 64 piksele), zaczynając od wyestymowanego punktu, opisującego środek oka. 
3. Region stanowi input sieci neuronowej, której struktura jest dokładniej opisana [[#Struktura sieci|tutaj]].
4. **Output** stanowi 5 punktów (x,y) z układu współrzędnych dużego zdjęcia.  Środek oka i cztery wierzchołki tęczówki oraz 16 punktów konturu oka
5. Współrzędne *x,y* konturów oka z [[#Face mesh]] zastępujemy tymi wyliczonymi przez sieć. Oś *z* pozostaje bez zmian. 
6. 5 punktów, dla każdego oka, dodajemy do siatki opisującej geometrię twarzy. 


# Struktura sieci 
Praca naukowa opisująca sieć neuronową o podobnej strukturze:
https://arxiv.org/pdf/1907.05047.pdf


#  Estymacja przesunięcia źrenicy awatara 
Zastosowaniem opisanej w artykule estymacji jest przewidywanie położenia źrenic avatara. Nie jest to dokładnie to czego szukamy, ale może stanowić inspirację. 
1. My chcemy estymować wektor kierunku wzroku, oni chcą przewidywać położenie źrenicy określając cztery kierunki przesunięcia. Następnie dla każdego kierunku obliczan jest skalar [0,1] określający jak bardzo źrenica jest przesunięta w tym kierunku.
2. Dla każdego kierunku wybierane są 2 punkty, których odległość najlepiej opisuje kierunek (dla patrzenia w prawo będzie to środek źrenicy oraz prawy kącik oka)
3. Obliczana jest odległość pomiędzy nimi $D_{current}$. Porównujemy ją z odległościami otrzymanymi empirycznie: $D_{neutral}$, $D_{activated}$. Pierwszy to odległość mierzona kiedy oko nie jest skierowane w danym kierunku, a drugi kiedy jest maksymalnie skierowane w danym kierunku. Z porównania otrzymujemy skalar [0,1]
4. Wariancja wewnątrzpopulacyjna odległości, pomiędzy znakami szczególnymi twarzy, stanowi problem ($D_{neutral}$, $D_{activated}$). Jako rozwiązanie proponowany jest krok, kalibrujący przesunięcia w czasie rzeczywistym. Wykorzystany do tego jest algorytm [Z-score](https://en.wikipedia.org/wiki/Standard_score) Z kilkoma modyfikacjami:

![](https://i.imgur.com/PDTEAnr.png)


# Zbiór danych do trenowania sieci neuronowej 
 **Cel**: otrzymanie macierzy położeń (2D) punktów na około oczu 
 
 **Zbiór danych**: 20 000 ręcznie opisanych zdjęć, poddanych obróbce (obrót, przesunięcie, nasycenie kolorów, szum, [redukcja wymiarów z zachowaniem dystansów](https://www.sciencedirect.com/topics/computer-science/nonlinear-mapping), dodawanie sztucznego szumu kamery)

**Metody**: trening odbywał się przez 250 epok korzystając z [Adam optimizer](https://arxiv.org/pdf/1412.6980.pdf)

**Funkcja straty**:  Mean Squared Distance normalized by the Inter-Eye Distance. Podobne podejście opisane jest w:
https://arxiv.org/pdf/1907.06724.pdf


